fun main(): Unit {
    var x: Int = 0;

    while (true) {
        if (x > 7) {
            break;
        }
        
        for (y: Int in 2 until x) {
            if (x / 2 == y) {
                continue;
            }
            if (x % y == 0) {
                break;
            }
            println(y);
        }   
        x++;
    }
}