fun main(): Unit {
    var x: Int = 42;
    var y: Boolean = true;
    var z: String = "Hello Kotlin";

    println(x);
    println(24);

    println(y);
    println(false);

    println(z);
    println("Bye Kotlin");    
}