fun fact(x: Int): Int {
    if (x < 2) return 1;
    else return x * fact(x - 1);
}

fun printTwice(x: Int): Unit {
    println(x);
    println(x);
    return Unit;
}

fun doNothing(): Unit {
    return;
}

fun main(): Unit {
    println(fact(7));
    printTwice(42);
    doNothing();
    // no return; needed
}