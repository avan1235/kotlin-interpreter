fun getAdder(x: Int): (Int) -> Int {
    return fun(y: Int): Int {
        return x + y;
    };
}

fun applyFor(x: Int, f: (Int) -> Int): Int {
    println("before applying in applyFor");
    return f(x);
}

fun getClosure(): () -> Unit {
    val name: String = "catched by closure";
    val closure: () -> Unit = fun (): Unit {
        println(name);
    };
    println("Closure created");
    return closure;
}

fun triple(x: Int): Int {
    return 3 * x;
}

fun highOrder(f1: (Int) -> Int, f2: (Int) -> Int): (Int) -> Int {
    return fun (x: Int): Int {
        return f1(x) * f2(x);
    };
}

fun main(): Unit {
    val lifeAdder: (Int) -> Int = getAdder(42);
    println(lifeAdder(42));
    println(lifeAdder(0));

    val anonDouble: (Int) -> Int = fun (x: Int): Int {
        return 2 * x;
    };
    println(applyFor(42, anonDouble));
    println(applyFor(42, ::triple));

    val tripleAsVal: (Int) -> Int = ::triple;
    println(applyFor(42, highOrder(anonDouble, tripleAsVal)));

    val closureFun: () -> Unit = getClosure();
    val name: String = "outside closure";
    closureFun();
    println(name);
}