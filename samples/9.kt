val x: Int = 0;
var y: Int = 0;

fun printGlobals(): Unit {
    println(x);
    println(y);
}

fun main(): Unit {
    println(x);
    println(y);
    printGlobals();

    // cannot reassign x = 1;
    y = 1;
    println(y);
    printGlobals();

    // vals and vars shadowing
    val x: Int = 2;
    var y: Int = 2;
    println(x);
    println(y);

    // conflicting declaration in the same scope
    // var x: Int = 3;
    // val x: Int = 3;

    // non conflicting declaration in other scope
    run {
        val x: Int = 3;
        var y: Int = 3;
        println(x);
        println(y);
    };

    run {
        val z: Int = 4;
        println(z);
    };

    // x here is not modified by run block operations
    println(x);
    println(y);
    // println(z); cannot be called as z is not visible
    printGlobals();
}