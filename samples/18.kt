fun smallEven(maxValue: Int): Sequence<Int> {
    return sequence<Int> {
        for (i: Int in 0 until maxValue + 1 step 2) yield(i);
    };
}

fun highOrderSeqeunce(number: Int): Sequence<Sequence<Int>> {
    return sequence<Sequence<Int>> {
        for (i: Int in 1 until number) {
            yield(sequence<Int> {
                for (j: Int in 1 until i + 1) yield(i * j);
            });
        }
    };
}

fun functionalSequence(): Sequence<(Int) -> Int> {
    return sequence<(Int) -> Int> {
        for (x: Int in 5..9) {
            yield(fun (y: Int): Int { return x * y; });
        }
    };
}

fun main(): Unit {
    val smallOdd: Sequence<Int> = sequence<Int> {
        for (i: Int in 0 until 5) {
            if (i % 2 == 0) continue;
            println("before yield");
            yield(i);
            println("after yield");
        }
        println("finished sequence");
    };

    for (x: Int in smallOdd) {
        println(x);
    }

    println("---");
    for (y: Int in smallEven(10)) {
        println(y);
    }

    println("---");
    for (seq: Sequence<Int> in highOrderSeqeunce(4)) {
        for (z: Int in seq) {
            println(z);
        }
    }

    println("---");
    for (f: (Int) -> Int in functionalSequence()) {
        println(f(42));
    }
}