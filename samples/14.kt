fun modifyArray(array: Array<Int>): Unit {
    for (i: Int in 0 until array.size) {
        if (i % 2 == 0) {
            array[i] = 0;
        }
    }
}

fun main(): Unit {

    val array: Array<Int> = arrayOf<Int>(1, 2, 3);
    println(array[0]);

    for (num: Int in array) {
        println(num);
    }

    modifyArray(array);

    for (num: Int in array) {
        println(num);
    }

    val matrix: Array<Array<Int>> = arrayOf<Array<Int>>(
        arrayOf<Int>(1, 3),
        arrayOf<Int>(2, 4),
        arrayOf<Int>(24, 42)
    );
    for (a: Array<Int> in matrix) {
        for (i: Int in a)
            println(i);
    }

    val initArray: Array<Int> = Array<Int>(4, fun (i: Int): Int { return i; });

    for (x: Int in 0 until initArray.size) {
        println(initArray[x]);
    }
}