fun main(): Unit {
    var x: Int = 37;
    var y: Int = 42;

    if (x < y) {
        println("Too small");
    }

    if (x == y) {
        println("Equal");
    }
    else {
        println("Not equal");
    }

    while (x < y) {
        println("Let's increase!");
        x++;
    }

    if (x == y) println("Now equal");
    while (x > 0) x--;
    println("x zeroed");
}