fun main(): Unit {
    var a: Int = 42;
    var b: Int = 24;
    var c: Int = a + b;
    var d: Int = a - b;
    var e: Int = a * b;
    var f: Int = a / b;
    var g: Int = a % b;
    var h: Int = -a;

    var i: Boolean = true;
    var j: Boolean = false;
    var k: Boolean = i && j;
    var l: Boolean = i || j;
    var m: Boolean = a < b;
    var n: Boolean = a <= b;
    var o: Boolean = a > b;
    var p: Boolean = a >= b;
    var q: Boolean = a == b;
    var r: Boolean = a != b;
    var s: Boolean = !i;

    var t: String = "String";
    var u: String = "Literal";
    var v: String = t + " " + u;
}