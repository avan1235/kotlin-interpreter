fun sum(x: Int, y: Int): Int {
    return x + y;
}

fun xor(x: Boolean, y: Boolean): Boolean {
    return (x || y) && !(x && y);
}

fun hello(str: String): String {
    return "Hello " + str;
}

fun main(): Unit {
    println(sum(24, 42));
    println(xor(false, true));
    println(hello("Kotlin"));
}