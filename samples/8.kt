fun sample(arg: Boolean): Unit {
    // cannot reassign arg = false;
    println(arg);
}

fun sampleShadowedAsVal(arg: Boolean): Unit {
    val arg: Boolean = false;
    println(arg);
    // cannot reassign arg = true;
}

fun sampleShadowedAsVar(arg: Boolean): Unit {
    var arg: Boolean = false;
    println(arg);
    arg = true;
    println(arg);
}

fun main(): Unit {
    val x: Int = 0;
    var y: Int = 0;
    // cannot reassign x = 1;
    y = 1;

    for (z: Int in -4 until 4) {
        println(z);
    }

    for (z: Int in 0 - 4 until 4 + 0 step 2 * 3) {
        println(z);
    }

    sample(true);
    sampleShadowedAsVal(true);
    sampleShadowedAsVar(true);
}