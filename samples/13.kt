fun main(): Unit {
    fun nested(): String { 
        return "String from nested function";
    }
    println(nested());

    var x: Int = 42;
    fun staticX(): Int {
        return x;
    }
    println(staticX());
    x = 24;
    run {
        var x: Int = 42;
        println(staticX());
        println(x);
    };
    println(x);
}