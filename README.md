# Kotlin Interpreter

This project provides an implementation of Kotlin language interpreter written in Haskell.
It uses simplified grammar of Kotlin language expressed in BNF that requires usage of 
semicolons after each statement and contains only subset of functionalities of Kotlin language.
Anyway, the number of programs that can be executed with the interpreter is pretty bug, see to 
the [samples](./samples) to see what can be actually achieved using this port of Kotlin language. 🕶

## Documentation

The language specification in [documentation file](./README.pdf). It contains the description of the 
functionalities implemented as a part of this language. WE can mention here the basic data types but
also the `yield` operator with the subset of sequences, arrays, lambda functions and higher-order functions.
The static analyzer analyzes also types of the all variables and is able to check if they will be valid without
running the program.

Language has elegant way of sending messages about errors that includes parsing errors as well as the runtime
errors (e.g. index out of bound error for arrays).

Language supports overriding identifiers with static binding as it works in the official Kotlin language and
passing the variables to functions in two approaches - as values for basic types and as references for arrays.

## Build instructions

To build interpreter executable file it should be enough to call `make` in main project directory.

If you don't have dependencies installed try to install missing ones from [install_deps.sh](./scripts/install_deps.sh)
script but try also to read the error messages and just analyze them - it's always the most proper way 😉.

## Usage

After valid build, `interpreter` file will appear in the root project directory. It can be run on some `.kt` files
with `./interpreter <source_file> [<source_file>]`.

## Testing

Interpreter is tested with the original Kotlin compiler by executing all files from [test](./test) directories 
and comparing them to the output of Kotlin compiler. The tests can be run with single command `make test`.
There are about 50 tests of all supported features for the language in order to make sure that the implementation
works as expected.
