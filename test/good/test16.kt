// Shadowing variables can change their types

fun main(): Unit {

    val x: Int = 42;
    println(x + x);

    run {
        var x: String = "42";
        println(x + x);
    };
    
    println(x);
}