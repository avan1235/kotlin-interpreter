// Local declaration take precedence over outer with highest priority

fun f(x: Int): Int {
    return x + 1;
}

fun main(): Unit {

    val f: (Int) -> String = fun (x: Int): String {
        return "const";
    };

    val x: String = f(41);

    println(x);
}