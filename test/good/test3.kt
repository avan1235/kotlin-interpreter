// Function must return its signature value

fun t1(): Unit {
    
}

fun t2(): Unit {
    return Unit;
}

fun t3(): Unit {
    return;
}

fun t4(): Int {
    return 0;
}

fun t5(): String {
    return "";
}

fun t6(): Boolean {
    return true;
}

fun t7(): Array<Array<Int>> {
    return arrayOf<Array<Int>>(
        arrayOf<Int>(1, 2),
        arrayOf<Int>(3, 4, 5)
    );
}

fun t8(): Array<() -> Unit> {
    return arrayOf<() -> Unit>(::t1, ::t2, ::t3);
}

fun t9(): Sequence<String> {
    return sequence<String> {
        for (i: Int in 0 until 9) {
            yield("tik");
        }
    };
}

fun t10(): Sequence<String> {
    return sequence<String> {

    };
}

fun main(): Unit {
    val a: Unit = t1();
    val b: Unit = t2();
    val c: Unit = t3();
    val d: Int = t4();
    val e: String = t5();
    val f: Boolean = t6();
    val g: Array<Array<Int>> = t7();
    val h: Array<() -> Unit> = t8();
    val i: Sequence<String> = t9();
    val j: Sequence<String> = t10();
    println(d);
    println(e);
    println(f);

    for (a: Array<Int> in g) {
        for (x: Int in a) {
            println(x);
        }
    }
    for (s: String in i) println(s);
}
