// Function declaration is included in its envronment

fun fib(x: Int): Int {
    if (x < 2) return 1;
    else return fib(x - 1) + fib(x - 2);
}

fun main(): Unit {
    println(fib(7));
}