// Nested loops skips work correctly

fun main(): Unit {
    var x: Int = 0;

    while (true) {
        if (x > 7) {
            println("break with x");
            break;
        }
        
        for (y: Int in 2 until x) {
            if (x / 2 == y) {
                println("conitnue with x and y");
                continue;
            }
            if (x % y == 0) {
                println("break with x and y");
                break;
            }
            var z: Int = y;
            while(z > 0) {
                z--;
                if (z % 4 == 3) continue;
                if (z == 1) break;

                println(x);
                println(y);
                println(z);
            }
            println(y);
        }
        println(x);
        x++;
    }
}