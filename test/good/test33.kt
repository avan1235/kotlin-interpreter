// Hidden variables can also be initialized when out of shadow

fun main(): Unit {

    val x: Int;

    if (4 < 5) {
        x = 4;
        println(x);
        val x: Int = 5;
        println(x);
    }
    else {
        run {
            val x: Int = 42;
            println(x);
        };
        x = 3;
    }
    println(x);
}