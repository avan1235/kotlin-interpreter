// Allow for break and continue inside a loop

fun main(): Unit {
    for (i: Int in 5 until 9 step 1) {
        if (i % 2 == 0) {
            continue;
        }
        println(i);
    }

    var x: Int = 6;
    while (x > 0) {
        x--;
        println(x);
        if (x == 3) continue;
        println(x);
        if (x == 1) {
            break;
        }
        println(x);
    }
}