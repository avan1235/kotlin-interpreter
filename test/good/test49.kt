// Genrators can be used multiple times

fun printSeq(s: Sequence<Int>): Unit {
    for (x: Int in s) {
        println(x);
    }
}

fun main(): Unit {

    val s: Sequence<Int> = sequence<Int> {
        var l: Int = 1;
        var n: Int = 2;

        while(true) {
            println("before yield");
            yield(n);
            println("after yield");


            val nn: Int = l + n;
            l = n;
            n = nn;

            if (n > 100) {
                break;
            }
        }

        yield(42);
    };

    printSeq(s);
    printSeq(s);
    printSeq(s);
}