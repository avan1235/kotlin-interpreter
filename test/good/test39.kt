// Variables and their assignments

fun main(): Unit {
    var x: Int;
    var y: Boolean;
    var z: String;
    
    z = "Hello Kotlin";
    y = true;
    x = 42;

    println(x);
    println(y);
    println(z);

    x = 24;
    y = false;
    z = "Bye Kotlin";

    println(x);
    println(y);
    println(z);
}