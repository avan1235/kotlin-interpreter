// Initialized variables usage

fun main(): Unit {
    val x: Int = 1;
    var y: Int;

    y = 2;
    println(x);
    println(y);

    val z: Int = x + y;
    y = z;
    println(x);
    println(y);
    println(z);
}