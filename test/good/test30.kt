// Initialization in both branches of if else must take place

fun main(): Unit {
    val x: Int;

    if (4 < 5) {
        run {
            val x: Int = 43;
            println(x);
        };
        x = 4;
    }
    else {
        x = 5;
    }

    println(x);    
}