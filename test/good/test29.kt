// Initialization in both branches of if else must take place

fun main(): Unit {
    val x: Int;

    if (4 < 5) {
        run {
            x = 4;
        };
    }
    else {
        run {
            x = 5;
        };
    }

    println(x);    
}