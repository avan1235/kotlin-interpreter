// Nested funcitons with static binding

fun main(): Unit {

    fun nested1(): Unit {

        var v1: Int = 0;
        println(v1);

        fun nested2(): Unit {

            v1 = v1 + 1;
            println(v1);
            var v2: Int = 1;
            fun nested3(): Unit {

                v1 = v1 + 1;
                v2 = v2 + 1;
                nested2();
                
                println(v1);
                println(v2);
            }
            v1 = v1 + 1;
            v2 = v2 + 1;
            if (v1 + v2 < 1000) {
                nested3();
            }
            println(v1);
            println(v2);
        }
        v1 = v1 + 1;
        nested2();
        println(v1);
    }
    nested1();

    var x: Int = 42;
    fun staticX(): Int {
        return x;
    }
    println(staticX());
    x = 24;
    run {
        var x: Int = 42;
        println(staticX());
        println(x);
    };
    println(x);
    println(staticX());
}