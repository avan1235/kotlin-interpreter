// Functions can use any number of anytype parameters

fun sum(x: Int, y: Int): Int {
    return x + y;
}

fun xor(x: Boolean, y: Boolean): Boolean {
    return (x || y) && !(x && y);
}

fun hello(str: String): String {
    return "Hello " + str;
}

fun useIt(seq: Sequence<Sequence<Int>>): Unit {
    for (seq: Sequence<Int> in seq) {
        println("consume outer");
        for (x: Int in seq) {
            println("consume inner");
            println(x);
        }
    }
}

fun generateSeq(): Sequence<Sequence<Int>> {
    return sequence<Sequence<Int>> {
        for (s: Int in 0..10) {
            println("before outer yield");
            yield(sequence<Int> {
                for (i: Int in 0 until 42) {
                    println("before inner yield");
                    yield(i);
                    println("after inner yield");
                }
            });
            println("after outer yield");
        }
    };
}

fun main(): Unit {
    println(sum(24, 42));
    println(xor(false, true));
    println(hello("Kotlin"));
    useIt(generateSeq());
}