// Function declaration take precedence over lambdas

fun main(): Unit {

    fun f(x: Int): Int {
        return x + 1;
    }

    val f: (Int) -> String = fun (x: Int): String {
        return "const";
    };

    val x: Int = f(41);

    println(x);
}