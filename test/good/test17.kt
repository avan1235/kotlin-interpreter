// Take into account only values returned in function body 

fun main(): Unit {
    if (0 == 1) {
        return;
    }
    val f: () -> Int = fun (): Int {
        return 5;
    };
    println(f());
}