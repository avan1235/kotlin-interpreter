// Can fully compare Int, Boolean, String types

fun main(): Unit {
    println(4 < 2);
    println(4 > 2);
    println(4 <= 2);
    println(4 >= 2);
    println(4 == 2);
    println(4 != 2);

    println(false < true);
    println(false > true);
    println(false <= true);
    println(false >= true);
    println(false == true);
    println(false != true);

    println("false" < "true");
    println("false" > "true");
    println("false" <= "true");
    println("false" >= "true");
    println("false" == "true");
    println("false" != "true");
}