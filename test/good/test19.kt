// Collect return types from inner blocks

fun f(): Int {
    val x: Int = 5;
    if (x < 0) {
        return 3;
    }
    run {
        run {
            run {
                return 5;
            };
        };
    };
    return 42;
}

fun main(): Unit {
    println(f());
    run {
        run {
            run {
                return Unit;
                println(f());
            };
            return;
            println(f());
        };
        return;
        println(f());
    };
    println(f());
    return Unit;
}