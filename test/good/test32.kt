// Initialization is also properly checked in lambdas

fun main(): Unit {

    val z: Int = 9;

    val x: () -> Unit = fun (): Unit {
        var x: Int;

        if (z > 0) {
            x = z;
        }
        else {
            x = -z;
        }
        println(x);
        println(z);
    };
    
    return Unit;
}