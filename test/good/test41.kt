// Functions with most parametrs passed by
// value when arrays passed by reference

fun fact(x: Int): Int {
    if (x < 2) return 1;
    else return x * fact(x - 1);
}

fun printTwice(x: Int): Unit {
    println(x);
    println(x);
    return Unit;
}

fun doNothing(): Unit {
    println("in doNothing");
    return;
}

fun modifyContent(a: Array<Int>): Unit {
    a[1] = 42;
}

fun main(): Unit {
    println(fact(7));
    printTwice(42);
    doNothing();

    val a: Array<Int> = arrayOf<Int>(1, 2, 3);
    for (x: Int in a) println(x);
    modifyContent(a);
    for (x: Int in a) println(x);

    // no return; needed
}