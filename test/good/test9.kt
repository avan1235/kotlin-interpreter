// Function can be hidden locally

fun f(): Unit {
    println("global");
}

fun main(): Unit {
    f();

    fun f(): Unit {
        println("local");
    }

    f();

    run {
        f();
        fun f(): Unit {
            println("in block");
        }
        f();
    };

    for (i: Int in 0 until 3) {
        f();
        fun f(): Unit {
            println(i);
        }
        f();
    }

    var i: Int = 3;
    while (i > 0) {
        fun f(): Unit {
            println(i);
        }
        i = i - 1;
        f();
    }
}