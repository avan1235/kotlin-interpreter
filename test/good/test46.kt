// Arrays usage and passing them by reference

fun modifyArray(array: Array<Int>): Unit {
    for (i: Int in 0 until array.size) {
        if (i % 2 == 0) {
            array[i] = 0;
        }
    }
}

fun printArray(a: Array<Int>): Unit {
    for (x: Int in a) println(x);
}

fun printArray(a: Array<String>): Unit {
    for (x: String in a) println(x);
}

fun printArray(a: Array<Array<Boolean>>): Unit {
    for (aa: Array<Boolean> in a) {
        for (x: Boolean in aa) println(x);
    }
}

fun main(): Unit {

    val a: Array<String> = arrayOf<String>("1", "2", "3");
    val b: Array<Int> = Array<Int>(100, fun (i: Int): Int {
        return i;
    });
    val c: Array<Array<Boolean>> = Array<Array<Boolean>>(1000, fun (i: Int): Array<Boolean> {
        if (i % 2 == 0) return arrayOf<Boolean>(true, false, true);
        else return arrayOf<Boolean>(false, true, false);
    });

    printArray(a);
    printArray(b);
    printArray(c);

    modifyArray(b);

    printArray(a);
    printArray(b);
    printArray(c);
}