// Function args can be hidden

fun useMyArgs(x: Int, y: String): String {
    var x: Int = 42;
    while (x > 0) {
        println(x);
        x--;
    }
    val y: String = "shadow";
    return y;
}

fun main(): Unit {
    println(useMyArgs(24, "hello"));
}