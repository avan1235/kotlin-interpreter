// Global variables mus be initalized

val g: String = "hello global world";
var h: Int = 42;

fun main(): Unit {

    println(g);
    println(h);

    run {
        val g: String;
        var h: Int;

        g = "now local initialized";
        h = 43;

        println(g); 
        println(h); 
    };

    println(g); 
    println(h); 
}