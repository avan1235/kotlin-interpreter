// Initializing is also checked in other functions

fun notMain(x: Int): String {
    val x: Int;

    if (false) {
        x = 1;
    }
    else {
        x = 0;
    }

    println(x);

    val y: Int;

    run {
        run {
            run {
                y = 42;
            };
            println(y);
        };
        println(y);
    };

    return "empty";
}

fun main(): Unit {
    println(notMain(42));
}