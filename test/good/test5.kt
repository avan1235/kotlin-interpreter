// Function args can be used

fun useMyArgs(x: Int, y: String): String {
    var x: Int = x;
    while (x > 0) {
        println(x);
        x--;
    }
    return y;
}

fun main(): Unit {
    println(useMyArgs(42, "hello"));
}