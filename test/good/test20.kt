// Collect yield types from inner blocks

fun f(): Sequence<Int> {
    return sequence<Int> {
        for (i: Int in 0 until 3) {
            if (i % 2 == 0) {
                yield(2 * i);
            }
            else {
                yield(3 * i);
            }
        }
        yield(42);
    };
}

fun main(): Unit {
    val s: Sequence<Int> = f();
    for (x: Int in s) {
        println(x);
    }
}