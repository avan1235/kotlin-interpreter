// Break and continue only closest loop

fun main(): Unit {

    var x: Int = 50;
    
    for (z: Int in 5..10) {
        while (x > 0) {
            var y: Int = x;
            println(y);
            while (y > 10) {
                println(x);
                println(y);
                if (y == 42) break;
                y--;
            }
            x--;
            println(x);
            if (x % 2 == 0) {
                continue;
            }
            println(x);
        }
        println(z);
    }

}