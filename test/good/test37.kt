// Merge sort naive implementation with copying the arrays

fun merge(l: Array<Int>, r: Array<Int>): Array<Int> {
    val a: Array<Int> = Array<Int>(l.size + r.size, fun (i: Int): Int { return 0; });
    
    var i: Int = 0; var j: Int = 0; var k: Int = 0;
    
    while (i < l.size && j < r.size) {
        if (l[i] < r[j]) {
            a[k] = l[i];
            i++;
        }
        else {
            a[k] = r[j];
            j++;
        }
        k++;
    }
    
    while (i < l.size) { a[k] = l[i]; i++; k++; }
    while (j < r.size) { a[k] = r[j]; j++; k++; }
    
    return a;
}

fun mergeSort(a: Array<Int>): Array<Int> {
    if (a.size < 2) return a;
    fun initWithA(from: Int): (Int) -> Int {
        return fun (i: Int): Int { return a[from + i]; };
    }
    val half: Int = a.size / 2;
    val l: Array<Int> = mergeSort(Array<Int>(half, initWithA(0)));
    val r: Array<Int> = mergeSort(Array<Int>(a.size - half, initWithA(half)));
    return merge(l, r);
}

fun main(): Unit {
    fun printArray(a: Array<Int>): Unit {
        for (i: Int in a) println(i);
    }

    val a: Array<Int> = arrayOf<Int>(3, 5, 4, 6, 8, 7, 1, 2, 9, 0);
    printArray(a);
    printArray(mergeSort(a));
    

    val bigA: Array<Int> = Array<Int>(1000,
        fun (i: Int): Int { return 999 - i; }
    );
    printArray(bigA);
    printArray(mergeSort(bigA));
}