// Can create array of sequences as higher order structure

fun main(): Unit {

    fun generateSeq(i: Int): Sequence<Int> {
        return sequence<Int> {
            var c: Int = i % 2;
            while (c < i) {
                if (c % 2 == i % 2) yield(c);
                c++;
            }
        };
    }

    val a: Array<Sequence<Int>> = Array<Sequence<Int>>(10, ::generateSeq);

    for (i: Int in 0 until a.size) {
        for (x: Int in a[i]) {
            println(x);
        }
    } 
}