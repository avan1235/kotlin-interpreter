// Base loops and conditional blocks

fun main(): Unit {
    var x: Int = 37;
    var y: Int = 42;

    if (x < y) {
        println("Too small");
    }

    if (x == y) {
        println("Equal");
    }
    else {
        println("Not equal");
    }

    while (x < y) {
        println("Let's increase!");
        x++;
    }

    if (x == y) println("Now equal");
    while (x > 0) x--;
    println("x zeroed");

    for (i: Int in 0..10) println(i);
    for (i: Int in 0 until 10) println(i);
    for (i: Int in 0 until 10 step 3) println(i);

    while (x < 100) {
        y = 42;
        while (y > 0) {
            println(y);
            y--;
        }
        println(x);
        x++;
    }
}