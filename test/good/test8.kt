// println function can also be hidden

fun println(s: String): Unit {
    println(42); // original function with other signature
    return;
}

fun main(): Unit {
    println("test println");
}