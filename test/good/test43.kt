// Properly handle global and local variables 
// with static idents binding

val x: Int = 0;
var y: Int = 0;

fun printGlobals(): Unit {
    println(x);
    println(y);
}

fun changeGlobal(yChanged: Int): Int {
    println(y);
    val y: Int = y;
    println(y);
    return y;
}

fun main(): Unit {
    println(x);
    println(y);
    printGlobals();

    // cannot reassign x = 1;
    y = 1;
    println(y);
    printGlobals();

    // vals and vars shadowing
    val x: Int = 2;
    var y: Int = 2;
    println(x);
    println(y);

    // non conflicting declaration in other scope
    run {
        val x: Int = 3;
        var y: Int = 3;
        println(x);
        println(y);
    };

    run {
        val z: Int = 4;
        println(z);
    };

    // x here is not modified by run block operations
    println(x);
    println(y);
    // println(z); // cannot be called as z is not visible
    printGlobals();

    println(y);
    println(changeGlobal(424));
    println(y);
}
