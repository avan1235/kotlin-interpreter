// Take into account only values yielded in inner scope 

fun main(): Unit {

    val s: Sequence<Int> = sequence<Int> {

        for (i: Int in 0 until 3) {
            yield(i);
        }

        val q: Sequence<String> = sequence<String> {
            var x: Int = 2;
            while (x > 0) {
                yield("two");
                x--;
            }
        };

        for (s: String in q) {
            yield(42);
        }
    };
    
    for (x: Int in s) {
        println(x);
    }
}