// Initialization in nested both branches of if else must take place

fun main(): Unit {
    val x: Int;

    if (4 < 5) {
        if (false) {
            println("hello");
            x = 4;
        }
        else {
            println(false);
            x = -4;
        }
    }
    else {
        x = 5;
    }

    println(x);    
}