// Initialization in both branches of if else must take place

fun main(): Unit {
    val x: Int;

    if (4 < 5) {
        println("hello");
        x = 4;
    }
    else {
        println("world");
        x = 5;
    }

    println(x);    
}