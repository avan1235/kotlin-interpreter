// Val can be initialized in inner, inner, inner... block

fun main(): Unit {
    val x: Int;

    run {
        run {
            run {
                run {
                    run {
                        x = 3;
                    };
                };
            };
        };
    };

    println(x);    
}
