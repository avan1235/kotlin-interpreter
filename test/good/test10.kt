// Function declaration take precedence over lambdas

fun f(x: Int): Int {
    return x + 1;
}

val f: (Int) -> Int = fun (x: Int): Int {
    return x + 2;
};

fun main(): Unit {

    val x: Int = f(41);

    println(x);
}