// Local declaration take precedence over outer 

fun f(x: Int): Int {
    return x + 1;
}

fun main(): Unit {

    fun f(x: Int): String {
        return "local";
    }

    val x: String = f(41);

    println(x);
}