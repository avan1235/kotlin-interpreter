// Main function with proper signature exists

fun f(): Int {
    println("out");
    return 0;
}

fun main(): Unit {

    val f: (Int) -> Boolean = fun (x: Int): Boolean {
        println("inner");
        return true;
    };

    val x: Int = f();
    println(x);    
}