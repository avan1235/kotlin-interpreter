// Initialize array with properly resolved funtion reference

fun twiced(l: Int): Array<Int> {
    val d: (Int) -> Int = fun (i: Int): Int { return 2 * i; };
    return Array<Int>(l, d);
}

fun twiced(l: String): String {
    return l + l;
}

fun main(): Unit {

    val x: Array<Array<Int>> = Array<Array<Int>>(10, ::twiced);

    for (row: Array<Int> in x) {
        for (v: Int in row) {
            println(v);
        }
        println("---");
    }
}