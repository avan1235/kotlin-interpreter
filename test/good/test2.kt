// Many main function but single proper signature

fun main(x : Int): Unit {
    println(x);
}

fun main(): Unit {
    println("this is called");
}

fun main(s: String): Boolean {
    println(true);
    return true;
}

fun main(x: Array<Unit>): String {
    println("array arg");
    return "";
}