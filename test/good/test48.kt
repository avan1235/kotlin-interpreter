// Functions used as parameters and variables

fun foldl(l: Array<Int>, acc: Int, f: (Int, Int) -> Int): Int {
    var acc: Int = acc; // by default acc is val - we shadow this param
    for (x: Int in l) {
        acc = f(acc, x);
    }
    return acc;
}

fun indexed(i: Int): Int {
    return i + 1;
}

fun main(): Unit {

    fun sum(a: Int, b: Int): Int {
        return a + b;
    }
    val mult: (Int, Int) -> Int = fun (a: Int, b: Int): Int {
        return a * b;
    };

    val a: Array<Int> = Array<Int>(10, ::indexed);

    println(foldl(a, 0, ::sum));
    println(foldl(a, 1, mult));

    val sumSaved: (Int, Int) -> Int = ::sum;
    println(foldl(a, 0, sumSaved)); 
}