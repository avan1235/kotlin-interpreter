// Val can be initialized in inner block

fun main(): Unit {
    val x: Int;

    run {
        x = 3;
    };

    println(x);    
}

