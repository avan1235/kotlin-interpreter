// Find function by reference

fun main(): Unit {

    fun f(x: Int): Int {
        return x + 1;
    }

    val f_x: (Int) -> Int = ::f;
    val x_f: Int = f(41);
    val x_f_x: Int = f_x(41);

    println(x_f);
    println(x_f_x);
}