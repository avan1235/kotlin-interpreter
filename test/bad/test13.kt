// Bad arguments in function call used - bad types

fun test(x: Int, s: String): Int {
    println(s);
    return x + 42;
}

fun main(): Unit {
    println(test(5, 5));
}