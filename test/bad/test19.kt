// Array must be initialized with funciton of proper signature

fun main(): Unit {

    val initWith: (Int) -> Int = fun (i: Int): Int {
        println("should return string to init array properly");
        return i;
    };

    val a: Array<String> = Array<String>(42, initWith);

    for (x: String in a) {
        println(x);
    }
}