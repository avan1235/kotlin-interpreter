// Bad arguments in function call used - too few

fun test(x: Int, y: Int): Int {
    return x + y;
}

fun main(): Unit {
    println(test(5));
}