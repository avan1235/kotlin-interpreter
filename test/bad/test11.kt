// Bad arguments in function call used - too many

fun test(x: Int): Int {
    return x + 42;
}

fun main(): Unit {
    println(test(5, "5"));
}