// Variable may be unintialized

fun main(): Unit {
    val x: Int;
    if (4 < 5) {
        x = 4;
    }
    else {
        val x: Int = 5;
    }

    println(x);
}