// Cannot return inside sequence body

fun main(): Unit {

    val s: Sequence<Int> = sequence<Int> {
        for (i: Int in 5..10) {
            return "nil";
        }
    };

    for (x: Int in s) {
        println(x);
    }
}