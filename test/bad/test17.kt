// Types in constructed array must match array declaration

fun main(): Unit {

    val a: Array<Int> = arrayOf<Int>("1", 2, "3");

    for (x: Int in a) println(x);
}