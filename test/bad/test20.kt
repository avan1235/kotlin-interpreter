// Cannot shadow variable twice

fun test(i: Int): Int {
    val i: Int = 2 * i;
    val i: Int = 2 * i;
    return i;
}


fun main(): Unit {

    test(42);
}