// Cannot access array out of bounds index [runtime error]

fun main(): Unit {

    val a: Array<Int> = arrayOf<Int>(1, 2, 3);

    for (i: Int in 0 until a.size) println(a[i]);

    println(a[-1]);
    
    for (i: Int in 0 until a.size) println(a[i]);
}