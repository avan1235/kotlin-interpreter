// Array must be initialized with funciton of proper signature

fun initWith(i: Int): Int {
    println("should return string to init array properly");
    return i;
}

fun main(): Unit {

    val a: Array<String> = Array<String>(42, ::initWith);

    for (x: String in a) {
        println(x);
    }
}