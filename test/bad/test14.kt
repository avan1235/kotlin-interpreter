// Cannot return bad type in funciton body

fun test(x: Int): Int {
    if (x < 5) {
        return "false";
    }
    return true;
}

fun main(): Unit {
    test(3);
}