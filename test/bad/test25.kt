// Cannot create array with negative size [runtime error]

fun main(): Unit {

    val s: Int = 42 - 84;

    println(s);

    val a: Array<String> = Array<String>(s,
        fun (i: Int): String { return "empty"; }
    );

    println(s);
}