// Cannot mod by 0 - runtime error [runtime error]

fun main(): Unit {
    val x: Int = 42;
    val y: Int = 42 - 42;

    println(x);
    println(y);
    println(x % y);
    println(x);
    println(y);
}