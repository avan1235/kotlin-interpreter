// Cannot yield bad type inside sequence body

fun main(): Unit {

    val s: Sequence<Int> = sequence<Int> {
        for (i: Int in 5..10) {
            yield("bad");
        }
    };

    for (x: Int in s) {
        println(x);
    }
}