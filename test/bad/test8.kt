// Types not match in while

fun main(): Unit {
    val x: String = "some string";
    
    while (x) {
        println("here");
    }
}