# List of goals not corresponding to file names.

.PHONY : all clean doc test

# Default goal.

all : interpreter

# Rules for building the parser.

grammar : Kotlin.cf
	bnfc --haskell -d --functor Kotlin.cf && \
	rm -rf ./src/Kotlin && \
	mv ./Kotlin ./src/

parser : grammar
	happy --ghc --coerce --array --info src/Kotlin/Par.y

lexer : grammar
	alex --ghc src/Kotlin/Lex.x

interpreter : src/* parser lexer
	cd src && \
	ghc --make Main.hs -o ../interpreter

create-env :
	scripts/create_env.sh

clean :
	scripts/clean.sh

doc : create-env
	scripts/generate_doc.sh

test-grammar :
	scripts/test_grammar.sh

test-interpreter : create-env
	scripts/test_interpreter.sh

test : test-grammar test-interpreter
