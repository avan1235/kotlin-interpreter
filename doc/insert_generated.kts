import java.io.File

val extraNotes = """
\subsection*{After notes}

This grammar consists single conflict that is connected with the "Dangling else" problem.
It is automatically resolved in expected way because generated parser prefers shift over reduce operation.
"""

val generated = File("../Kotlin.tex").readText()
val commands = generated.run {
    val begin = indexOf("\\newcommand")
    val end = indexOf("\n", lastIndexOf("\\newcommand"))
    substring(begin, end)
}
val doc = generated.run {
    val begin = indexOf("\\section")
    val end = indexOf("\\end{document}")
    substring(begin, end).trim()
}

val docFile = File("README.tex")
val markdown = docFile.readText().run {
    val index = indexOf("\n", lastIndexOf("\\newcommand"))
    val before = substring(0, index)
    val after = substring(index)
    before + commands + after
}.run {
    val index = indexOf("\\end{document}")
    val before = substring(0, index)
    val after = substring(index)
    before + doc + extraNotes + after
}
docFile.writeText(markdown)
