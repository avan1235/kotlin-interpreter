import java.io.File

fun outFile(text: String): String = """
**Expected output**

```shell
$text
```"""

fun scriptFile(text: String): String = """
**Sample code**

```kotlin
$text
```"""

val samplesDir = File("../samples")
val replacements = samplesDir.listFiles()
    .filter { it.name.endsWith(".kt") }
    .map { it.name to it.readText().trim() }
    .filter { it.second.isNotEmpty() }
    .toMap()

var readme = File("README_TEMPLATE.md").readText()
val samples = replacements.keys.map { it.substringBefore('.') }.toSet()
samples.forEach {
    val replacement = "<!-- SOURCE $it -->"
    val insert = buildString {
        replacements["$it.kt"]?.let { appendLine(scriptFile(it)) }
        replacements["$it.out"]?.let { appendLine(outFile(it)) }
    }
    readme = readme.replace(replacement, insert)
}

File("README.md").writeText(readme)
