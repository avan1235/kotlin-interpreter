# Kotlin Language Features

Kotlin is an impertaive language that was proposed by JetBrains company.

It offers concise syntax and expressiveness without lossing the control over the types. In real life Kotlin code there is a concept of type inference for compile-time type information, meaning some type information in the code may be omitted.

For purpose of this task we assume that all types have to be given explicitly which results in valid Kotlin code. Also to make it simpler we assume that the statements in Kotlin have to have the semicolon `;` which is normally optional and nearly always missed in code. We assume that the entry point of the program is the `main` function which has to be defined as `fun main(): Unit` (as standard Kotlin allows also for `fun main(args: Array<String>): Unit` to be used).

## Implemented features

### Implemented features summary

| **Implemented part description**       | **Points** |
|----------------------------------------|------------|
| Base interpreter features              | 15         |
| Extended interpreter features          | 5          |
| Static typing                          | 4          |
| Nested function definitions            | 2          |
| `Int` indexed arrays                   | 1          |
| `break` and `continue` statements      | 1          |
| Functions as parameters                | 4          |
| Generating procedures                  | 3          |
| **Sum of points**                      | **35**     |


### Base language interpreter features

1. Base types.

<!-- SOURCE 1 -->

2. Literals, arithmetic and comparisons. All of them work as in Kotlin (so also as in Java) excepting the `String` concatenation `+` operator in this implementation works only when both sides expression of the operator are of type `String`.

<!-- SOURCE 2 -->

3. Variables and an assignment operation for `var`.

<!-- SOURCE 3 -->

4. The built-in `println` procedure for variables of base types.

<!-- SOURCE 4 -->

5. `while`, `if`, `if` / `else` statements

<!-- SOURCE 5 -->

6. Functions (also returning `Unit` which don't need the `return;` statement but can be used as well as `return Unit;`) and recursion.

<!-- SOURCE 6 -->

7. Two ways of passing parameters:
- base types are passed always by value
- arrays (described below) are passed by reference

8. Final variables. By default all functions' and procedures' parameters are final so they cannot be reasigned until they are shadowed in function body. Also the `for` loop arguments are final and they also cannot be reasigned but can be shadowed in the `for` statement block.

<!-- SOURCE 8 -->

### Extended language interpreter features

9. Overriding identifiers with static binding (local and global variables and nested procedures and functions). As in full Kotlin language the `{ }` stands for the lambda expression of type `() -> Unit` (which is not included in this interpreter implementation) there is special command `run` which allows to run the lambda in place of creation (that's why it's named in this way). Such operation allows to create nested code blocks that can be used to create a new scope and restrict the visibility of the variables.

<!-- SOURCE 9 -->

10. Handling runtime errors (e.g. division by zero) with elegant message and interpreter stop. 

11. Functions working with any types and number of arguments (also inlcuding the arrays and sequences described below).

<!-- SOURCE 10 -->

### Extra language interpreter features

12. Static typing - always terminating phase of type control checking before running the interpreted program.

13. Nested function definitions with maintaining the correctness of static binding of identifiers.

<!-- SOURCE 13 -->

14. `Int` indexed arrays that are passed by reference to functions. They have the `.size` property and can be iterated in `for` loop easily. The arrays can consist of other arrays which have the elements of the same type. The arrays are created with special `arrayOf` function or with `Array` constructor which takes the size and the lambda indexed generator as parameters.

<!-- SOURCE 14 -->

16. Operations that break the `while` loop - `break` and `continue` statements. 

<!-- SOURCE 16 -->

17. Functions as parameters - function can be returned as a function result as well as it can be an argument to some function. There is special syntax in Kotlin for creating anonymous functions which is not implemented in interpreter (using `{ }`) but there is possibility to create the anonymous function using `fun` keyword and define the function signature in proper way. Named function can be referenced using `::` and we assume that their type has to match the type of their usage (so we cannot pass the `(Int) -> String` function in place of `(Int) -> Unit` function which in full Kotlin implementation is allowed and just ignores the result of function call). Function closures can be achived with the usage of anonymous function definitions. Functions applications are resolved according to the same rules as in full language specification (it has to be done really carefully as not only being funtion vs variable matters when searching for function to apply but also the fact, if this function and variable are already shadowed in current scope)

<!-- SOURCE 17 -->

18. Generating procedures and syntax to use them with the usage of sequence abstraction.

<!-- SOURCE 18 -->