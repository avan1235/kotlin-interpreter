#!/bin/bash

sudo apt update &&
sudo apt install alex happy ghc git make pandoc  \
                 texlive-latex-base texlive-fonts-recommended texlive-fonts-extra texlive-latex-extra &&
curl -sSL https://get.haskellstack.org/ | sh &&
export PATH=$PATH:~/.local/bin

stack install BNFC &&
echo "Add ~/.local/bin to your PATH to make it working"
