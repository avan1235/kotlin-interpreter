#!/bin/bash

kotlinc_bin="kotlinc/bin/kotlinc"

rm -f README.pdf
bnfc --latex Kotlin.cf 

echo "Generate programs output"

for file in `ls ./samples/*.kt | sort -V`; do
    out_file="${file%.kt}.out"
    jar_file="${file%.kt}.jar"

    echo "Generate output for $file..."

    $kotlinc_bin $file -include-runtime -d $jar_file &&
    java -jar $jar_file >$out_file 2>&1
done

echo "Inserting data to template"


kotlinc_bin_from_doc="../${kotlinc_bin}"
cd doc &&
$kotlinc_bin_from_doc -script insert_sources.kts &&
pandoc --standalone --from markdown --to latex README.md > README.tex &&
$kotlinc_bin_from_doc -script insert_generated.kts &&
pdflatex README.tex &&
mv README.pdf ../
