#!/bin/bash

rm -rf doc/*.tex \
       doc/*.aux \
       doc/*.log \
       doc/*.out \
       doc/*.tex \
       doc/*.aux \
       doc/*.log \
       doc/*.out \
       doc/*.bak \
       doc/README.md \
       samples/*.out \
       samples/*.jar \
       test/good/*.out \
       test/good/*.jar \
       test/bad/*.out \
       test/bad/*.jar \
       src/Kotlin/ \
       src/*.hi \
       src/*.o \
       interpreter \
       Kotlin.tex \
       README.pdf \
       kotlinc \
       kotlin.zip \
       .hs
