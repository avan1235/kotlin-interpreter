#!/bin/bash

kotlinc_bin="kotlinc/bin/kotlinc"

function exit_on_error {
    if [ $1 -ne 0 ]; then
        echo "Failed on stage: $2"
        exit 1
    fi
}

function test_good {
    out_interpreter="${1%.kt}.i.out"
    out_kotlin="${1%.kt}.k.out"
    jar_file="${1%.kt}.jar"

    echo "Testing interpreter on $1..."

    ./interpreter $1 > $out_interpreter
    if [ $? -ne 0 ]; then
        echo "Expected 0 exit code in $1"
        exit 1
    fi

    $kotlinc_bin $1 -include-runtime -d $jar_file
    exit_on_error $? "compile by kotlin"
    
    java -jar $jar_file >$out_kotlin 2>&1
    exit_on_error $? "run with java"

    diff $out_interpreter $out_kotlin

    if [ $? -ne 0 ]; then
        echo "Expected outputs to be equal after running $1"
        exit 1
    fi

    rm $out_interpreter $out_kotlin
}

function test_bad {
    echo "Expecting error exit from interpreter on $1..."

    ./interpreter $1
    if [ $? -eq 0 ]; then
        echo "Expected non zero exit code in $1"
        exit 1
    fi
}

if [ $# -eq 0 ]; then
    flags="--bad --samples --good"
else
    flags="$*"
fi

make interpreter
exit_on_error $? "build from sources"

if [[ $flags == *--bad* ]]; then
    for file in `ls ./test/bad/*.kt | sort -V`; do
        test_bad $file
    done
fi

if [[ $flags == *--samples* ]]; then
    for file in `ls ./samples/*.kt | sort -V`; do
        test_good $file
    done
fi

if [[ $flags == *--good* ]]; then
    for file in `ls ./test/good/*.kt | sort -V`; do
        test_good $file
    done
fi

echo "All tests passed"
exit 0
