#!/bin/bash

function clean_exit {
    mv Makefile.copy Makefile
    exit $1
}

function test_valid_parsing {
    ./Kotlin/Test < $1
    if [ $? -ne 0 ]; then
        echo "Error in parsing $1!!!"
        clean_exit 1
    fi
}

mv Makefile Makefile.copy
rm -rf ./Kotlin/ && \
bnfc --haskell --functor -d -m Kotlin.cf && \
make

for file in ./samples/*.kt; do
    test_valid_parsing $file
done

for file in ./test/good/*.kt; do
    test_valid_parsing $file
done

echo "Success parsing"

c="$( happy -gca ./Kotlin/Par.y 2>&1 | grep conflicts | sed 's/^.*: //' | awk -F: '{s+=$1} END{print s}' | sed 's/ *$//g' )"
if [ "$c" = "1" ]; then
    echo "Found 1 conflict in grammar (expected)"
else
    echo "Found $c conflicts in grammar (unexpected)"
    clean_exit 1
fi

clean_exit 0
