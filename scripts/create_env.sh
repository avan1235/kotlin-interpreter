#!/bin/bash

download_url="https://github.com/JetBrains/kotlin/releases/download/v1.5.0/kotlin-compiler-1.5.0.zip"

wget $download_url -O kotlin.zip
unzip -o kotlin.zip

