module Util where

import qualified Data.Map as Map
import System.Exit (ExitCode (ExitFailure), exitWith)

compose :: [a -> a] -> a -> a
compose = foldr (.) id

mapMapValues :: Ord k => (v1 -> v2) -> Map.Map k v1 -> Map.Map k v2
mapMapValues f m = Map.fromAscList $ map (mapSnd f) $ Map.toAscList m

mapSnd :: (b -> b') -> (a, b) -> (a, b')
mapSnd m (f, s) = (f, m s)

fstT :: (a, b, c) -> a
fstT (a, _, _) = a

sndT :: (a, b, c) -> b
sndT (_, b, _) = b

thdT :: (a, b, c) -> c
thdT (_, _, c) = c

eqLen :: [a] -> [b] -> Bool
eqLen l1 l2 = length l1 == length l2

exitFailureMsg :: String -> Int -> String -> IO ()
exitFailureMsg file code msg = do
  putStrLn $ "Failure in " ++ file
  putStrLn msg
  exitWith $ ExitFailure code
