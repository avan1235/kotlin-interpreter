module TypeChecker (typecheck) where

import Constants
import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.Reader
import Control.Monad.State
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe
import qualified Data.Set as Set
import Debug.Trace
import ErrorMessage
import Kotlin.Abs
import TypeEnvironment
import Util

typecheck :: Program -> Either String ()
typecheck program =
  runIdentity $
    runExceptT $
      (`runReaderT` emptyTCEnv) $
        (`evalStateT` emptyState) $
          typeOfP program

defChange :: Def -> TCM TCStmtChange
defChange (DDefFun pos n args t block) = do
  let at = map argCType args
  let rt = asCType t
  let envArgsChange = compose $ map addTCArgVal args
  let envFunChange = addHiddenFunToTCEnv n at rt
  let envChange = markNoLoop . markTCEnvCanBeHidden . envFunChange . envArgsChange
  fr <- askFunEnv n
  oi <- getOI
  inCleanState $
    local envChange (typeOfB block rt True)
      >> validateReturns pos rt
  case fr of
    Nothing -> return (addHiddenFunToTCEnv n at rt, oi)
    Just defined -> case maybeFunType defined at of
      Nothing -> return (addHiddenFunToTCEnv n at rt, oi)
      Just (_, CanBeHidden) -> return (addHiddenFunToTCEnv n at rt, oi)
      Just (_, Hidden) -> throwError $ functionRedefinition pos
defChange (DDefVar pos n t e) = defVarVal pos n t e Var
defChange (DDefVal pos n t e) = defVarVal pos n t e Val

declChange :: Decl -> TCM TCStmtChange
declChange (DDeclVar pos n t) = declVarVal pos n t Var NotInit
declChange (DDeclVal pos n t) = declVarVal pos n t Val NotInit

typeOfP :: Program -> TCM ()
typeOfP (ProgramDef pos []) = do
  let (n, args, rt) = entryPoint
  fr <- askDefinedFun n pos
  case maybeFunCType fr args of
    Just t | t == rt -> return ()
    _ -> throwError $ noValidMainFunction pos
typeOfP (ProgramDef pos (def : rest)) = do
  envChange <- fst <$> defChange def
  cleanOI
  local envChange $ typeOfP $ ProgramDef pos rest

typeOfE :: Expr -> TCM CType
typeOfE (ELitUnit _) = return CTUnit
typeOfE (EString _ _) = return CTString
typeOfE (ELitInt _ _) = return CTInt
typeOfE (EMul pos e1 _ e2) = typeOfBinaryOp "Int" [intOp] e1 e2 pos
typeOfE (EAdd pos e1 _ e2) = typeOfBinaryOp "Int or String" [intOp, stringOp] e1 e2 pos
typeOfE (ERel pos e1 _ e2) = typeOfBinaryOp "Int, String or Boolean" [intCompOp, stringCompOp, booleanOp] e1 e2 pos
typeOfE (ENeg pos e) = typeOfUnaryOp "Int" CTInt e pos
typeOfE (ELitTrue _) = return CTBoolean
typeOfE (ELitFalse _) = return CTBoolean
typeOfE (EAnd pos e1 e2) = typeOfBinaryOp "Boolean" [booleanOp] e1 e2 pos
typeOfE (EOr pos e1 e2) = typeOfBinaryOp "Boolean" [booleanOp] e1 e2 pos
typeOfE (ENot pos e) = typeOfUnaryOp "Boolean" CTBoolean e pos
typeOfE (EArray pos t e) = do
  let tr = asCType t
  ers <- mapM typeOfE e
  if all (isOneOf tr) ers
    then return $ CTArray tr
    else throwError $ arrayElemBadType pos
typeOfE (EArrayInit pos t es ei) = do
  esr <- typeOfE es
  eir <- typeOfE ei
  let tr = asCType t
  let fSign = CTFun ([CTInt], tr)
  let vr = isOneOf eir fSign
  case (esr, vr) of
    (CTInt, True) -> return $ CTArray tr
    (_, False) -> throwError $ invalidArrayInitFunctionType pos
    _ -> throwError $ invalidArraySizeType pos
typeOfE (EArraySize pos n) = do
  vr <- askInitVarVal n pos
  case vr of
    CTArray _ -> return CTInt
    _ -> throwError $ invalidSizePropertyUsage pos
typeOfE (EArrayGet pos n e) = do
  vr <- askInitVarVal n pos
  ir <- typeOfE e
  case (vr, ir) of
    (CTArray t, CTInt) -> return t
    _ -> throwError $ invalidArrayAccess pos
typeOfE (EVarVal pos n) = askInitVarVal n pos
typeOfE (EFun pos n) = do
  fr <- askDefinedFun n pos
  return $ CTMultFun $ possFunSigns fr
typeOfE (ESeq pos t block) = do
  let rt = asCType t
  inCleanState $
    typeOfB block CTUnit False
      >> validateYields pos rt
  return $ CTSeq rt
typeOfE (EFunDef pos args t block) = do
  let at = map argCType args
  let rt = asCType t
  let envArgsChange = compose $ map addTCArgVal args
  let envChange = markNoLoop . markTCEnvCanBeHidden . envArgsChange
  inCleanState $
    local envChange (typeOfB block rt True)
      >> validateReturns pos rt
  return $ CTFun (at, rt)
typeOfE (EApp pos n args) = do
  fr <- askFunEnv n
  vr <- askVarValEnv n
  ar <- mapM typeOfE args
  case (fr, vr) of
    (Just fSign, _) | isTCDefFunWithSign fSign ar && Hidden == tcFunVis fSign ar -> return $ funCType fSign ar
    (Just fSign, Just (CTFun vSign, _, Init, Hidden))
      | isTCDefFunWithSign fSign ar && CanBeHidden == tcFunVis fSign ar ->
        let var = funArgsCType vSign
         in if eqLen ar var && all isOneOfP (zip ar var)
              then return $ funRetCType vSign
              else return $ funCType fSign ar
    (_, Just (CTFun vSign, _, Init, _)) | ar == funArgsCType vSign -> return $ funRetCType vSign
    (Just fSign, _) | isTCDefFunWithSign fSign ar -> return $ funCType fSign ar
    _ -> throwError $ notDeclaredFunction pos

typeOfC :: CtrlStructBody -> TCM TCStmtChange
typeOfC (SSingle _ stmt) = local markTCEnvCanBeHidden (typeOfS stmt)
typeOfC (SMulti _ stmt) = local markTCEnvCanBeHidden (typeOfB stmt CTUnit False)

typeOfB :: Block -> CType -> Bool -> TCM TCStmtChange
typeOfB (SBlock pos []) rt reqRet = do
  (rr, yr, oi) <- get
  case (rr, rt) of
    ([], CTUnit) | reqRet -> put ([CTUnit], yr, oi) >> noTCEnvChange
    ([], _) | reqRet -> throwError $ missingReturn pos
    _ -> noTCEnvChange
typeOfB (SBlock pos (stmt : rest)) rt reqRet = do
  (envChange, oi) <- typeOfS stmt
  putOI oi
  (envChange', oi') <- local envChange $ typeOfB (SBlock pos rest) rt reqRet
  putOI oi'
  return (envChange' . envChange, oi')

typeOfS :: Stmt -> TCM TCStmtChange
typeOfS (SEmpty _) = noTCEnvChange
typeOfS (SDefDecl _ defDecl) = do
  case defDecl of
    DDDef _ d -> defChange d
    DDDecl _ d -> declChange d
typeOfS (SAss pos n e) = do
  vr <- askVarValEnv n
  er <- typeOfE e
  case vr of
    Just s@(t, Var, _, _)
      | isOneOf er t -> initVarVal n s
      | otherwise -> throwError $ expressionNotMatchVarType pos
    Just s@(t, Val, NotInit, _)
      | isOneOf er t -> initVarVal n s
      | otherwise -> throwError $ expressionNotMatchVarType pos
    _ -> throwError $ invalidAssignment pos
typeOfS (SAssArr pos n i e) = do
  vr <- askInitVarVal n pos
  er <- typeOfE e
  ir <- typeOfE i
  case vr of
    CTArray ar | isOneOf er ar && ir == CTInt -> noTCEnvChange
    _ -> throwError $ invalidAssignment pos
typeOfS (SIncr pos n) = intOpCheck pos n
typeOfS (SDecr pos n) = intOpCheck pos n
typeOfS (SBreak pos) = requireInLoop pos
typeOfS (SContinue pos) = requireInLoop pos
typeOfS (SExp _ e) = typeOfE e >> noTCEnvChange
typeOfS (SRet _ e) = do
  er <- typeOfE e
  (rr, yr, oi) <- get
  put (er : rr, yr, oi)
  noTCEnvChange
typeOfS (SURet pos) = typeOfS (SRet pos $ ELitUnit pos)
typeOfS (SYield _ e) = do
  er <- typeOfE e
  (rr, yr, oi) <- get
  put (rr, er : yr, oi)
  noTCEnvChange
typeOfS (SIf pos e body) = booleanCheck pos e body
typeOfS (SWhile pos e body) = local markInLoop $ booleanCheck pos e body
typeOfS (SForExpr pos a e body) = do
  let ar = argCType a
  let envChange = markInLoop . addTCArgVal a
  er <- typeOfE e
  inCleanOI (local envChange $ typeOfC body)
  case er of
    (CTArray t) | isOneOf t ar -> noTCEnvChange
    (CTSeq t) | isOneOf t ar -> noTCEnvChange
    _ -> throwError $ invalidForExpr pos
typeOfS (SForRangeStep pos a ef et es body) = do
  let ar = argCType a
  let envChange = markInLoop . addTCArgVal a
  efr <- typeOfE ef
  etr <- typeOfE et
  esr <- typeOfE es
  inCleanOI (local envChange $ typeOfC body)
  case (efr, etr, esr, ar) of
    (CTInt, CTInt, CTInt, CTInt) -> noTCEnvChange
    _ -> throwError $ invalidForExpr pos
typeOfS (SForRange pos a ef et body) = typeOfS (SForRangeStep pos a ef et (ELitInt pos 1) body)
typeOfS (SForClosedRange pos a ef et body) = typeOfS (SForRange pos a ef et body)
typeOfS (SBlockRun pos block) = do
  (c, n) <- getOI
  c' <- envChange <$> inSeparatedOI (local markTCEnvCanBeHidden $ typeOfB block CTUnit False)
  updateEnvWithInits c' c n
typeOfS (SIfElse pos e tBody fBody) = do
  (c, n) <- getOI
  n' <- askNotInitVarVals
  env <- ask
  toi <- envChange <$> inSeparatedOI (typeOfC tBody)
  foi <- envChange <$> inSeparatedOI (typeOfC fBody)
  let c' = mergeInitChanges n' env toi foi
  putOI (c', n)
  er <- typeOfE e
  case er of
    CTBoolean -> updateEnvWithInits c' c n
    _ -> throwError $ notBooleanExpressionInCondition pos

declVarVal :: BNFC'Position -> Ident -> Type -> FinalState -> InitState -> TCM TCStmtChange
declVarVal pos n vt fs is = do
  vr <- askVarValEnv n
  oi <- getOI
  let t = asCType vt
  let r@(_, oi') = addHiddenTCVar n t fs is oi
  putOI oi'
  case vr of
    Nothing -> return r
    Just (_, _, _, CanBeHidden) -> return r
    _ -> throwError $ variableRedefinition pos

defVarVal :: BNFC'Position -> Ident -> Type -> Expr -> FinalState -> TCM TCStmtChange
defVarVal pos n vt e fs = do
  et <- typeOfE e
  let t = asCType vt
  if isOneOf t et
    then declVarVal pos n vt fs Init
    else throwError $ expressionNotMatchVarType pos

askVarValEnv :: Ident -> TCM (Maybe TCVarValState)
askVarValEnv n = asks $ Map.lookup n . venv
  where
    venv (v, _, _) = v

askFunEnv :: Ident -> TCM (Maybe TCFunSignEnv)
askFunEnv n = asks $ Map.lookup n . fenv
  where
    fenv (_, f, _) = f

askInitVarVal :: Ident -> BNFC'Position -> TCM CType
askInitVarVal n pos = do
  vr <- askVarValEnv n
  case vr of
    Just (t, _, Init, _) -> return t
    Just (_, _, NotInit, _) -> throwError $ notInitializedVariable pos
    _ -> throwError $ notDeclaredVariable pos

askNotInitVarVals :: TCM (Set.Set Ident)
askNotInitVarVals = do
  vr <- asks $ Map.toList . venv
  return $ Set.fromList $ map fst $ filter notInit vr
  where
    notInit (_, (_, _, NotInit, _)) = True
    notInit _ = False
    venv (v, _, _) = v

askDefinedFun :: Ident -> BNFC'Position -> TCM TCFunSignEnv
askDefinedFun n pos = do
  fr <- askFunEnv n
  case fr of
    Just sign -> return sign
    _ -> throwError $ notDeclaredFunction pos

requireInLoop :: BNFC'Position -> TCM TCStmtChange
requireInLoop pos = do
  (_, _, l) <- ask
  if l == Loop
    then noTCEnvChange
    else throwError $ usedBreakContinueOutOfLoop pos

typeOfBinaryOp :: String -> [((CType, CType), CType)] -> Expr -> Expr -> BNFC'Position -> TCM CType
typeOfBinaryOp n ts e1 e2 pos = do
  t1r <- typeOfE e1
  t2r <- typeOfE e2
  let m1 = filter (isOneOf t1r . fst . fst) ts
  let m2 = filter (isOneOf t2r . snd . fst) ts
  case (m1, m2) of
    ([(_, rt1)], [(_, rt2)]) | rt1 == rt2 -> return rt1
    ([], _) -> throwError $ binaryOperatorBadType n pos
    (_, []) -> throwError $ binaryOperatorBadType n pos
    _ -> throwError $ fatalState pos

typeOfUnaryOp :: String -> CType -> Expr -> BNFC'Position -> TCM CType
typeOfUnaryOp n t e pos = do
  er <- typeOfE e
  if isOneOf t er
    then return t
    else throwError $ unaryOperatorBadType n pos

booleanCheck :: BNFC'Position -> Expr -> CtrlStructBody -> TCM TCStmtChange
booleanCheck pos e body = do
  er <- typeOfE e
  case er of
    CTBoolean -> inCleanOI (typeOfC body) >> noTCEnvChange
    _ -> throwError $ notBooleanExpressionInCondition pos

intOpCheck :: BNFC'Position -> Ident -> TCM TCStmtChange
intOpCheck pos n = do
  vr <- askVarValEnv n
  case vr of
    Just (CTInt, Var, Init, _) -> noTCEnvChange
    Just (CTInt, Val, Init, _) -> throwError $ invalidAssignment pos
    Just (CTInt, _, NotInit, _) -> throwError $ notInitializedVariable pos
    _ -> throwError $ unaryOperatorBadType "Int" pos

validateCollectedType :: ErrorMessage -> BNFC'Position -> CType -> [CType] -> [CType] -> TCM ()
validateCollectedType msg pos rt expect notExpect
  | null notExpect && all (rt ==) expect = return ()
  | otherwise = throwError $ msg pos

validateReturns :: BNFC'Position -> CType -> TCM ()
validateReturns pos rt = do
  (rr, yr, _) <- get
  validateCollectedType invalidReturnType pos rt rr yr

validateYields :: BNFC'Position -> CType -> TCM ()
validateYields pos rt = do
  (rr, yr, _) <- get
  validateCollectedType invalidYieldType pos rt yr rr

noTCEnvChange :: TCM TCStmtChange
noTCEnvChange = do
  oi <- getOI
  return (id, oi)

getOI :: TCM TCOutEnvInit
getOI = do
  (_, _, oi) <- get
  return oi

putOI :: TCOutEnvInit -> TCM ()
putOI oi = do
  (rr, yr, _) <- get
  put (rr, yr, oi)

cleanOI :: TCM ()
cleanOI = do
  (rr, yr, _) <- get
  put (rr, yr, emptyOI)
  return ()

inSeparatedOI :: TCM a -> TCM a
inSeparatedOI action = do
  n <- askNotInitVarVals
  oi <- getOI
  putOI (id, n)
  r <- action
  putOI oi
  return r

inCleanState :: TCM a -> TCM a
inCleanState action = do
  outState <- get
  put emptyState
  result <- action
  put outState
  return result

inCleanOI :: TCM a -> TCM a
inCleanOI action = do
  (rr, yr, oi) <- get
  put (rr, yr, emptyOI)
  result <- action
  (rr', yr', _) <- get
  put (rr' ++ rr, yr' ++ yr, oi)
  return result

isTCDefFunWithSign :: TCFunSignEnv -> [CType] -> Bool
isTCDefFunWithSign env args = any isDef poss
  where
    poss = signCombs args
    isDef = isDefFunWithSign (`Map.member` env)

updateEnvWithInits :: TCEnvChange -> TCEnvChange -> Set.Set Ident -> TCM TCStmtChange
updateEnvWithInits c' c n = return (c', (c' . c, n))

initVarVal :: Ident -> TCVarValState -> TCM TCStmtChange
initVarVal n s = do
  (c, si) <- getOI
  let c' = if Set.member n si then initVarValInTCEnv n s else id
  let oi = (c' . c, Set.delete n si)
  putOI oi
  return (initVarValInTCEnv n s, oi)

markTCEnvCanBeHidden :: TCEnv -> TCEnv
markTCEnvCanBeHidden (v, f, l) = (v', f', l)
  where
    v' = mapMapValues markVarCanBeHidden v
    f' = mapMapValues markFunCanBeHidden f
    markVarCanBeHidden (n, fs, is, _) = (n, fs, is, CanBeHidden)
    markFunCanBeHidden s = mapMapValues markFunSignCanBeHidden s
    markFunSignCanBeHidden (t, _) = (t, CanBeHidden)

markInLoop :: TCEnv -> TCEnv
markInLoop (v, f, _) = (v', f, Loop)
  where
    v' = mapMapValues markValAsWouldBeInit v
    markValAsWouldBeInit (n, fs, _, h) = (n, fs, Init, h)

markNoLoop :: TCEnv -> TCEnv
markNoLoop (v, f, _) = (v, f, NoLoop)

tcFunState :: TCFunSignEnv -> [CType] -> (CType, HideState)
tcFunState fr ar = state
  where
    possSigns = signCombs ar
    fromEnv k = Map.lookup k fr
    [state] = Maybe.mapMaybe fromEnv possSigns

funCType :: TCFunSignEnv -> [CType] -> CType
funCType fr ar = fst $ tcFunState fr ar

tcFunVis :: TCFunSignEnv -> [CType] -> HideState
tcFunVis fr ar = snd $ tcFunState fr ar
