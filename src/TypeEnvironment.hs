module TypeEnvironment where

import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.Reader
import Control.Monad.State
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe
import qualified Data.Set as Set
import Debug.Trace
import Kotlin.Abs
import Util

-- Base types definitions

data CType
  = CTInt
  | CTString
  | CTBoolean
  | CTUnit
  | CTSeq CType
  | CTArray CType
  | CTFun ([CType], CType)
  | CTMultFun (Map.Map [CType] CType)
  deriving (Eq, Ord, Show)

data IValue
  = IVInt Integer
  | IVString String
  | IVBoolean Bool
  | IVUnit
  | IVSeq CType (IContY -> ICont -> ICont)
  | IVArray CType ILoc
  | IVFun ([CType], CType) IFun
  | IVMultFun IFunSignEnv
  | IVArrayData (Map.Map Integer IValue)

data FinalState = Var | Val deriving (Eq, Ord, Show)

data InitState = Init | NotInit deriving (Eq, Ord, Show)

data HideState = Hidden | CanBeHidden deriving (Eq, Ord, Show)

data InLoopEnv = Loop | NoLoop deriving (Eq, Ord, Show)

type Op = ((CType, CType), CType)

-- TypeChecker types definitions

type TCM a = StateT TCState (ReaderT TCEnv (ExceptT String Identity)) a

type Returns = [CType]

type Yields = [CType]

type TCVarValState = (CType, FinalState, InitState, HideState)

type TCVarEnv = Map.Map Ident TCVarValState

type TCFunSignEnv = Map.Map [CType] (CType, HideState)

type TCFunEnv = Map.Map Ident TCFunSignEnv

type TCEnv = (TCVarEnv, TCFunEnv, InLoopEnv)

type TCEnvChange = TCEnv -> TCEnv

type TCState = (Returns, Yields, TCOutEnvInit)

type TCOutEnvInit = (TCEnvChange, Set.Set Ident)

type TCStmtChange = (TCEnvChange, TCOutEnvInit)

-- Interpreter types definitions

type IM a = (ExceptT String IO) a

type ILoc = Integer

type IStore = Map.Map ILoc IValue

type IAnswer = IStore

type IVarEnv = Map.Map Ident (ILoc, CType, HideState)

type IFun = [IValue] -> IContR -> ICont

type IFunSignEnv = Map.Map [CType] (CType, HideState, IFun)

type IFunEnv = Map.Map Ident IFunSignEnv

type IEnv = (IVarEnv, IFunEnv)

type IntOp = Integer -> Integer -> Integer

type ICont = IStore -> IM IAnswer

type IContB = ICont

type IContC = ICont

type IContR = IValue -> ICont

type IContY = IValue -> ICont

type IContE = IValue -> ICont

type IContD = IEnv -> ICont

type IConts = (IContB, IContC, IContR, IContY, IContD)

-- Helper functions  deifnitions

asCType :: Type -> CType
asCType t = case t of
  TInt _ -> CTInt
  TString _ -> CTString
  TBoolean _ -> CTBoolean
  TUnit _ -> CTUnit
  TSequence _ it -> CTSeq (asCType it)
  TArray _ it -> CTArray (asCType it)
  TFun _ ats rt -> CTFun (map asCType ats, asCType rt)

argName :: Arg -> Ident
argName (TypedIdent _ n _) = n

argCType :: Arg -> CType
argCType (TypedIdent _ _ t) = asCType t

funArgsCType :: ([CType], CType) -> [CType]
funArgsCType = fst

funRetCType :: ([CType], CType) -> CType
funRetCType = snd

addHiddenFunToTCEnv :: Ident -> [CType] -> CType -> TCEnvChange
addHiddenFunToTCEnv n at rt (v, f, l) = (v, f', l)
  where
    f' = Map.insert n newFuncs f
    newFuncs = Map.insert at (rt, Hidden) oldFuncs
    oldFuncs = Maybe.fromMaybe Map.empty $ Map.lookup n f

addHiddenTCVar :: Ident -> CType -> FinalState -> InitState -> TCOutEnvInit -> TCStmtChange
addHiddenTCVar n t fs is (c, s) = (envChange, (c, s'))
  where
    envChange (v, f, l) = (Map.insert n (t, fs, is, Hidden) v, f, l)
    s' = Set.delete n s

addTCArgVal :: Arg -> TCEnvChange
addTCArgVal a (v, f, l) = (v', f, l)
  where
    n = argName a
    v' = Map.insert n (argCType a, Val, Init, CanBeHidden) v

initVarValInTCEnv :: Ident -> TCVarValState -> TCEnvChange
initVarValInTCEnv n (t, vf, _, h) (v, f, l) = (v', f, l)
  where
    v' = Map.insert n (t, vf, Init, h) v

envChange :: TCStmtChange -> TCEnvChange
envChange = fst . snd

mergeInitChanges :: Set.Set Ident -> TCEnv -> TCEnvChange -> TCEnvChange -> TCEnvChange
mergeInitChanges ns env te fe = compose $ map initVarValIfInitInBoth $ Set.toList ns
  where
    isInit (_, _, Init, _) = True
    isInit _ = False
    stateInEnv n e = v Map.! n where (v, _, _) = e
    isInitInEnv n e = isInit $ stateInEnv n e
    initVarValIfInitInBoth n =
      if isInitInEnv n (te env) && isInitInEnv n (fe env)
        then initVarValInTCEnv n $ stateInEnv n env
        else id

maybeFunCType :: TCFunSignEnv -> [CType] -> Maybe CType
maybeFunCType fr ar = fst <$> Map.lookup ar fr

maybeFunType :: TCFunSignEnv -> [CType] -> Maybe (CType, HideState)
maybeFunType fr ar = Map.lookup ar fr

possFunSigns :: TCFunSignEnv -> Map.Map [CType] CType
possFunSigns fr = Map.fromList $ map extractType $ Map.toList fr
  where
    extractType (input, (output, _)) = (input, output)

isOneOf :: CType -> CType -> Bool
isOneOf (CTMultFun signs) (CTFun f) = f `elem` Map.toList signs
isOneOf t1@(CTFun f) t2@(CTMultFun signs) = f `elem` Map.toList signs
isOneOf t1 t2 = t1 == t2

isOneOfP :: (CType, CType) -> Bool
isOneOfP (t1, t2) = isOneOf t1 t2

signCombs :: [CType] -> [[CType]]
signCombs [] = [[]]
signCombs ((CTMultFun signs) : xs) = [CTFun s : cs | s <- Map.toList signs, cs <- signCombs xs]
signCombs (t : xs) = [t : cs | cs <- signCombs xs]

isDefFunWithSign :: ([CType] -> Bool) -> [CType] -> Bool
isDefFunWithSign inFunSignEnv ar = any inFunSignEnv possSigns
  where
    possSigns = signCombs ar
