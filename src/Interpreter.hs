module Interpreter (interpret) where

import Constants
import Control.Monad.Except
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe
import Debug.Trace
import ErrorMessage
import Kotlin.Abs
import TypeEnvironment
import Util

interpret :: Program -> IO (Either String IAnswer)
interpret program =
  runExceptT $
    evalP program emptyIEnv ed emptyIStore

evalP :: Program -> IEnv -> IContD -> ICont
evalP (ProgramDef pos []) (_, fenv) kd = f [] ke
  where
    (n, i, _) = entryPoint
    Just fSign = Map.lookup n fenv
    Just (_, _, f) = Map.lookup i fSign
    ke _ = return
evalP (ProgramDef pos (def : rest)) env kd = evalDefChange def env kd'
  where
    kd' env' = evalP (ProgramDef pos rest) env' kd

evalDefChange :: Def -> IEnv -> IContD -> ICont
evalDefChange (DDefFun _ n args t block) (venv, fenv) kd = kd env'
  where
    at = map argCType args
    rt = asCType t
    fSignEnv = Maybe.fromMaybe Map.empty $ Map.lookup n fenv
    fSignEnv' = Map.insert at (rt, Hidden, funDef) fSignEnv
    fenv' = Map.insert n fSignEnv' fenv
    env' = (venv, fenv')
    funDef vs kr s' = evalB block env''' True (ek, ek, kr, ey, ed) ek s'''
      where
        (s'', env'', ls) = allocArgVars args env' s'
        env''' = markIEnvCanBeHidden env''
        s''' = assignValues ls vs at s''
evalDefChange (DDefVar _ n t e) env kd = evalE e env ke
  where
    ct = asCType t
    ke er s' = kd env' $ Map.insert l (selectOneOf ct er) s''
      where
        (s'', env', l) = allocVar n ct env s'
evalDefChange (DDefVal pos n t e) env kd = evalDefChange (DDefVar pos n t e) env kd

evalDeclChange :: Decl -> IEnv -> IContD -> ICont
evalDeclChange (DDeclVar pos n t) env kd s = kd env' s'
  where
    (s', env', _) = allocVar n (asCType t) env s
evalDeclChange (DDeclVal pos n t) env kd s = evalDeclChange (DDeclVar pos n t) env kd s

evalDefDeclS :: DefDecl -> IEnv -> IContD -> ICont
evalDefDeclS (DDDecl _ d) = evalDeclChange d
evalDefDeclS (DDDef _ d) = evalDefChange d

evalB :: Block -> IEnv -> Bool -> IConts -> ICont -> ICont
evalB (SBlock _ []) _ reqRet (_, _, kr, _, _) k = if reqRet then kr IVUnit else k
evalB (SBlock pos (stmt : rest)) env reqRet ks@(kb, kc, kr, ky, _) k = evalS stmt env (kb, kc, kr, ky, kd') k'
  where
    kd' env' = evalB (SBlock pos rest) env' reqRet ks k
    k' = evalB (SBlock pos rest) env reqRet ks k

evalC :: CtrlStructBody -> IEnv -> IConts -> ICont -> ICont
evalC (SSingle _ stmt) env = evalS stmt (markIEnvCanBeHidden env)
evalC (SMulti _ stmt) env = evalB stmt (markIEnvCanBeHidden env) False

evalS :: Stmt -> IEnv -> IConts -> ICont -> ICont
evalS (SEmpty _) _ _ k = k
evalS (SAss _ n e) env _ k = evalE e env ke
  where
    l = getVarLoc env n
    t = getVarCType env n
    ke er s' = k $ Map.insert l (selectOneOf t er) s'
evalS (SDefDecl _ defDecl) env (_, _, _, _, kd) k = evalDefDeclS defDecl env kd
evalS (SAssArr _ n i e) env ks k =
  evalE i env (\ir -> evalE e env (\er -> ke (ir, er)))
  where
    ke (IVInt idx, er) s' = k $ Map.insert al a s'
      where
        IVArray t al = getVarVal env n s'
        Just (IVArrayData m) = Map.lookup al s'
        v = selectOneOf t er
        m' = Map.insert idx v m
        a = IVArrayData m'
evalS (SIncr _ n) env _ k = evalSingleIntOp succ n env k
evalS (SDecr _ n) env _ k = evalSingleIntOp pred n env k
evalS (SBreak _) _ (kb, _, _, _, _) _ = kb
evalS (SContinue _) _ (_, kc, _, _, _) _ = kc
evalS (SExp _ e) env _ k = evalE e env (const k)
evalS (SRet _ e) env (_, _, kr, _, _) k = evalE e env kr
evalS (SURet pos) env ks k = evalS (SRet pos $ ELitUnit pos) env ks k
evalS (SYield _ e) env (_, _, _, ky, _) k = evalE e env ke
  where
    ke er = ky er >=> k
evalS (SIf _ e body) env ks k = evalE e env ke
  where
    ke (IVBoolean b) = if b then evalC body env ks k else k
evalS (SIfElse _ e tBody fBody) env ks k = evalE e env ke
  where
    ke (IVBoolean b) =
      if b then evalC tBody env ks k else evalC fBody env ks k
evalS stmt@(SWhile _ e body) env ks@(_, _, kr, ky, kd) k = evalE e env ke
  where
    ke (IVBoolean b) = if b then evalC body env (kb', kc', kr, ky, kd) kc' else k
    kc' = evalS stmt env ks k
    kb' = k
evalS (SBlockRun pos block) env ks k = evalB block (markIEnvCanBeHidden env) False ks k
evalS (SForExpr _ a e body) env ks k = evalE e env ke
  where
    ke (IVArray t al) s' = evalArrayLoop (Map.elems m) l t body env' ks k s''
      where
        Just (IVArrayData m) = Map.lookup al s'
        (s'', env', l) = allocArgVar a env s'
    ke (IVSeq t seqDef) s' = evalSeqLoop seqDef l t body env' ks k s''
      where
        (s'', env', l) = allocArgVar a env s'
evalS (SForRangeStep pos a ef et es body) env ks k =
  evalE ef env (\fr -> evalE et env (\tr -> evalE es env (\sr -> ke (fr, tr, sr))))
  where
    ke (IVInt i, IVInt to, IVInt step) s' = evalForRangeLoop i to step l body env' ks k s''
      where
        (s'', env', l) = allocArgVar a env s'
evalS (SForRange pos a ef et body) env ks k = evalS (SForRangeStep pos a ef et (ELitInt pos 1) body) env ks k
evalS (SForClosedRange pos a ef et body) env ks k =
  evalE ef env (\fr -> evalE et env (\tr -> ke (fr, tr)))
  where
    ke (IVInt i, IVInt to) s' = evalForRangeLoop i (to + 1) 1 l body env' ks k s''
      where
        (s'', env', l) = allocArgVar a env s'

evalE :: Expr -> IEnv -> IContE -> ICont
evalE (ELitUnit _) _ ke = ke IVUnit
evalE (EString _ v) _ ke = ke $ IVString v
evalE (ELitInt _ i) _ ke = ke $ IVInt i
evalE (EMul pos e1 op e2) env ke =
  evalE e1 env (\er1 -> evalE e2 env (\er2 -> ke' (er1, op, er2)))
  where
    ke' (IVInt i1, OTimes _, IVInt i2) s = IVInt (i1 * i2) `ke` s
    ke' (IVInt i1, ODiv _, IVInt i2) s =
      if i2 == 0
        then throwError $ divideByZero pos
        else IVInt (i1 `div` i2) `ke` s
    ke' (IVInt i1, OMod _, IVInt i2) s =
      if i2 == 0
        then throwError $ modByZero pos
        else IVInt (i1 `mod` i2) `ke` s
evalE (EAdd _ e1 op e2) env ke =
  evalE e1 env (\er1 -> evalE e2 env (\er2 -> ke' (er1, op, er2)))
  where
    ke' (IVInt i1, OPlus _, IVInt i2) = ke $ IVInt $ i1 + i2
    ke' (IVInt i1, OMinus _, IVInt i2) = ke $ IVInt $ i1 - i2
    ke' (IVString i1, OPlus _, IVString i2) = ke $ IVString $ i1 ++ i2
evalE (ERel _ e1 comp e2) env ke =
  evalE e1 env (\er1 -> evalE e2 env (\er2 -> ke' (er1, er2)))
  where
    ke' (IVInt i1, IVInt i2) = ke $ IVBoolean $ asOp comp i1 i2
    ke' (IVString i1, IVString i2) = ke $ IVBoolean $ asOp comp i1 i2
    ke' (IVBoolean i1, IVBoolean i2) = ke $ IVBoolean $ asOp comp i1 i2
evalE (ENeg _ e) env ke = evalE e env ke'
  where
    ke' (IVInt i) = ke $ IVInt $ - i
evalE (ELitTrue _) _ ke = ke $ IVBoolean True
evalE (ELitFalse _) _ ke = ke $ IVBoolean False
evalE (EAnd _ e1 e2) env ke = evalBinaryBoolean (&&) e1 e2 env ke
evalE (EOr _ e1 e2) env ke = evalBinaryBoolean (||) e1 e2 env ke
evalE (ENot _ e) env ke = evalE e env ke'
  where
    ke' (IVBoolean b) = ke $ IVBoolean $ not b
evalE (EArrayInit pos t is fs) env ke =
  evalE is env (\ir -> evalE fs env (\fr -> ke' (ir, selectOneOf initFCType fr)))
  where
    ct = asCType t
    initFCType = CTFun ([CTInt], ct)
    ke' (IVInt i, IVFun _ f) s' =
      if i < 0
        then throwError $ invalidArraySize pos
        else createInitArray i f ct [] ke s'
evalE (EArray _ t vs) env ke = evalArgs vs [] env ke'
  where
    ct = asCType t
    ke' ers s' = a `ke` s''
      where
        (s'', a) = allocArrayData ers ct s'
evalE (EArraySize pos n) env ke = k
  where
    k s = ke (IVInt $ fromIntegral $ Map.size $ getArrayData env n s) s
evalE (EArrayGet pos n e) env ke = evalE e env ke'
  where
    ke' (IVInt i) s' = case Map.lookup i $ getArrayData env n s' of
      Just v -> ke v s'
      _ -> throwError $ indexOutOfBands i pos
evalE (EVarVal _ n) env ke = k
  where
    k s = ke (getVarVal env n s) s
evalE (EFun _ n) env ke = ke $ IVMultFun $ getIFunEnv env n
evalE (ESeq _ t block) env ke = ke $ IVSeq ct seqDef
  where
    ct = asCType t
    seqDef ky k = evalB block env False (ek, ek, er, ky, ed) k
evalE (EFunDef _ args t block) env ke = ke $ IVFun (at, rt) funDef
  where
    at = map argCType args
    rt = asCType t
    funDef vs kr s' = evalB block env'' True (ek, ek, kr, ey, ed) ek s'''
      where
        (s'', env', ls) = allocArgVars args env s'
        env'' = markIEnvCanBeHidden env'
        s''' = assignValues ls vs at s''
evalE (EApp pos n args) env ke = k
  where
    k s = evalArgs args [] env ke' s
      where
        ke' ers = f ers ke
          where
            ts = map iValCType ers
            f = resolveFun n ts env s

nextFreeLoc :: IStore -> ILoc
nextFreeLoc = fromIntegral . Map.size

allocArgVar :: Arg -> IEnv -> IStore -> (IStore, IEnv, ILoc)
allocArgVar a = allocVar n t
  where
    n = argName a
    t = argCType a

allocArgVars :: [Arg] -> IEnv -> IStore -> (IStore, IEnv, [ILoc])
allocArgVars = go []
  where
    go ls [] env s = (s, env, reverse ls)
    go ls (a : args) env s = go (l : ls) args env' s'
      where
        (s', env', l) = allocArgVar a env s

allocVar :: Ident -> CType -> IEnv -> IStore -> (IStore, IEnv, ILoc)
allocVar n t env s = (s', (venv', fenv), l)
  where
    l = nextFreeLoc s
    s' = Map.insert l IVUnit s
    (venv, fenv) = env
    venv' = Map.insert n (l, t, Hidden) venv

allocArrayData :: [IValue] -> CType -> IStore -> (IStore, IValue)
allocArrayData vs t s = (s', a)
  where
    al = nextFreeLoc s
    vs' = map (selectOneOf t) vs
    m = IVArrayData $ Map.fromAscList $ zip [0 ..] vs'
    s' = Map.insert al m s
    a = IVArray t al

assignValues :: [ILoc] -> [IValue] -> [CType] -> IStore -> IStore
assignValues [] [] [] s = s
assignValues (l : ls) (v : vs) (t : ts) s = assignValues ls vs ts $ Map.insert l (selectOneOf t v) s

evalArrayLoop :: [IValue] -> ILoc -> CType -> CtrlStructBody -> IEnv -> IConts -> ICont -> ICont
evalArrayLoop [] _ _ _ _ _ k s = k s
evalArrayLoop (x : xs) l ct body env ks@(_, _, kr, ky, kd) k s =
  evalC body env (kb', kc', kr, ky, kd) kc' s'
  where
    s' = Map.insert l (selectOneOf ct x) s
    kc' = evalArrayLoop xs l ct body env ks k
    kb' = k

evalSeqLoop :: (IContY -> ICont -> ICont) -> ILoc -> CType -> CtrlStructBody -> IEnv -> IConts -> ICont -> ICont
evalSeqLoop seqDef l ct body env ks@(_, _, kr, ky, kd) k = seqDef ky' k
  where
    k' = return
    ky' yr s = evalC body env (kb', kc', kr, ky, kd) k' s'
      where
        s' = Map.insert l (selectOneOf ct yr) s
        kc' = evalSeqLoop seqDef l ct body env ks k
        kb' = k

evalForRangeLoop :: Integer -> Integer -> Integer -> ILoc -> CtrlStructBody -> IEnv -> IConts -> ICont -> ICont
evalForRangeLoop i to step l body env ks@(_, _, kr, ky, kd) k s =
  if i < to then evalC body env (kb', kc', kr, ky, kd) kc' s' else k s
  where
    s' = assignValues [l] [IVInt i] [CTInt] s
    kc' = evalForRangeLoop (i + step) to step l body env ks k
    kb' = k

evalSingleIntOp :: (Integer -> Integer) -> Ident -> IEnv -> ICont -> ICont
evalSingleIntOp op n env k s = k s'
  where
    l = getVarLoc env n
    IVInt v = getVarVal env n s
    s' = Map.insert l (IVInt $ op v) s

createInitArray :: Integer -> IFun -> CType -> [IValue] -> IContE -> ICont
createInitArray 0 _ ct vs ke s = a `ke` s'
  where
    (s', a) = allocArrayData vs ct s
createInitArray i f ct vs ke s = k s
  where
    ke' er = createInitArray (i - 1) f ct (er : vs) ke
    k = f [IVInt (i - 1)] ke'

asOp :: Ord a => RelOp -> (a -> a -> Bool)
asOp (OLe _) = (<)
asOp (OLeq _) = (<=)
asOp (OGt _) = (>)
asOp (OGtq _) = (>=)
asOp (OEqu _) = (==)
asOp (ONEqu _) = (/=)

getVarVal :: IEnv -> Ident -> IStore -> IValue
getVarVal env n s = v
  where
    Just v = Map.lookup (getVarLoc env n) s

getArrayData :: IEnv -> Ident -> IStore -> Map.Map Integer IValue
getArrayData env n s = m
  where
    IVArray _ al = getVarVal env n s
    Just (IVArrayData m) = Map.lookup al s

selectOneOf :: CType -> IValue -> IValue
selectOneOf (CTFun (i, o)) (IVMultFun fSign) = IVFun (i, o) $ getIFun fSign i
selectOneOf _ v = v

getVarLoc :: IEnv -> Ident -> ILoc
getVarLoc (venv, _) n = l
  where
    Just (l, _, _) = Map.lookup n venv

getVarCType :: IEnv -> Ident -> CType
getVarCType (venv, _) n = t
  where
    Just (_, t, _) = Map.lookup n venv

evalArgs :: [Expr] -> [IValue] -> IEnv -> ([IValue] -> ICont) -> ICont
evalArgs [] l _ ke = ke $ reverse l
evalArgs (a : args) l env ke =
  evalE a env (\er -> evalArgs args (er : l) env ke)

evalBinaryBoolean :: (Bool -> Bool -> Bool) -> Expr -> Expr -> IEnv -> IContE -> ICont
evalBinaryBoolean op e1 e2 env ke =
  evalE e1 env (\er1 -> evalE e2 env (\er2 -> ke' (er1, er2)))
  where
    ke' (IVBoolean b1, IVBoolean b2) = ke $ IVBoolean $ op b1 b2

iValCType :: IValue -> CType
iValCType v = case v of
  IVInt _ -> CTInt
  IVString _ -> CTString
  IVBoolean _ -> CTBoolean
  IVUnit -> CTUnit
  IVSeq t _ -> CTSeq t
  IVArray t _ -> CTArray t
  IVFun t _ -> CTFun t
  IVMultFun t -> CTMultFun $ mapMapValues retCT t
  where
    retCT (rt, _, _) = rt

isIDefFunWithSign :: IFunSignEnv -> [CType] -> Bool
isIDefFunWithSign env = isDefFunWithSign (`Map.member` env)

iFunState :: IFunSignEnv -> [CType] -> (CType, HideState, IFun)
iFunState fr ar = state
  where
    possSigns = signCombs ar
    fromEnv k = Map.lookup k fr
    [state] = Maybe.mapMaybe fromEnv possSigns

iFunVis :: IFunSignEnv -> [CType] -> HideState
iFunVis fr ar = sndT $ iFunState fr ar

askIFunEnv :: Ident -> IFunEnv -> Maybe IFunSignEnv
askIFunEnv = Map.lookup

getIFunEnv :: IEnv -> Ident -> IFunSignEnv
getIFunEnv (_, fenv) n = fSign
  where
    Just fSign = Map.lookup n fenv

askIVarEnv :: Ident -> IVarEnv -> Maybe (ILoc, CType, HideState)
askIVarEnv = Map.lookup

getIFun :: IFunSignEnv -> [CType] -> IFun
getIFun fr ar = thdT $ iFunState fr ar

getVarFunWithBackup :: ILoc -> [CType] -> IStore -> IFun -> IFun
getVarFunWithBackup l ar s f = case Map.lookup l s of
  Just (IVFun vSign g) | ar == funArgsCType vSign -> g
  _ -> f

getReqVarFun :: ILoc -> [CType] -> IStore -> IFun
getReqVarFun l ar s = case Map.lookup l s of
  Just (IVFun vSign f) | ar == funArgsCType vSign -> f

resolveFun :: Ident -> [CType] -> IEnv -> IStore -> IFun
resolveFun n ar (venv, fenv) s = f
  where
    fr = askIFunEnv n fenv
    vr = askIVarEnv n venv
    f = case (fr, vr) of
      (Just fSign, _) | isIDefFunWithSign fSign ar && Hidden == iFunVis fSign ar -> getIFun fSign ar
      (Just fSign, Just (l, _, Hidden))
        | isIDefFunWithSign fSign ar && CanBeHidden == iFunVis fSign ar ->
          getVarFunWithBackup l ar s $ getIFun fSign ar
      (Just fSign, Nothing) | isIDefFunWithSign fSign ar -> getIFun fSign ar
      (Just fSign, _) | isIDefFunWithSign fSign ar -> getIFun fSign ar
      (_, Just (l, _, _)) -> getReqVarFun l ar s

markIEnvCanBeHidden :: IEnv -> IEnv
markIEnvCanBeHidden (v, f) = (v', f')
  where
    v' = mapMapValues markVarCanBeHidden v
    f' = mapMapValues markFunCanBeHidden f
    markVarCanBeHidden (l, t, _) = (l, t, CanBeHidden)
    markFunCanBeHidden s = mapMapValues markFunSignCanBeHidden s
    markFunSignCanBeHidden (t, _, b) = (t, CanBeHidden, b)