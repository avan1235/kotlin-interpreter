module ErrorMessage where

import Kotlin.Abs

type ErrorMessage = BNFC'Position -> String

notDeclaredFunction :: ErrorMessage
notDeclaredFunction = msgWithPos "Not declared function used"

notDeclaredVariable :: ErrorMessage
notDeclaredVariable = msgWithPos "Not declared variable used"

notInitializedVariable :: ErrorMessage
notInitializedVariable = msgWithPos "Not initialized variable used"

invalidArrayAccess :: ErrorMessage
invalidArrayAccess = msgWithPos "Invalid array access"

unaryOperatorBadType :: String -> ErrorMessage
unaryOperatorBadType t = msgWithPos $ "Unary " ++ t ++ " expression needs " ++ t

binaryOperatorBadType :: String -> ErrorMessage
binaryOperatorBadType t = msgWithPos $ "Binary " ++ t ++ " expression needs " ++ t

arrayElemBadType :: ErrorMessage
arrayElemBadType = msgWithPos "Array elem type not matches array declared type"

invalidSizePropertyUsage :: ErrorMessage
invalidSizePropertyUsage = msgWithPos "Cannot get .size property"

invalidArraySizeType :: ErrorMessage
invalidArraySizeType = msgWithPos "Invalid type of array size parameter in constructor"

invalidArrayInitFunctionType :: ErrorMessage
invalidArrayInitFunctionType = msgWithPos "Invalid type of array initialization function parameter in constructor"

invalidArraySize :: ErrorMessage
invalidArraySize = msgWithPos "Size of initialized array cannot be negative"

noValidMainFunction :: ErrorMessage
noValidMainFunction _ = "No main function defined in program scope"

notUniqueBlockType :: ErrorMessage
notUniqueBlockType = msgWithPos "Not unique type returned in block"

invalidAssignment :: ErrorMessage
invalidAssignment = msgWithPos "Invalid (re)assignment"

functionRedefinition :: ErrorMessage
functionRedefinition = msgWithPos "Duplicated function definition"

variableRedefinition :: ErrorMessage
variableRedefinition = msgWithPos "Duplicated variable definition"

expressionNotMatchVarType :: ErrorMessage
expressionNotMatchVarType = msgWithPos "Variable type doesn't match expression type"

notBooleanExpressionInCondition :: ErrorMessage
notBooleanExpressionInCondition = msgWithPos "Condition expression must evaluate to Boolean"

invalidForExpr :: ErrorMessage
invalidForExpr = msgWithPos "Invalid for loop expression"

invalidReturnType :: ErrorMessage
invalidReturnType = msgWithPos "Invalid return type of function"

missingReturn :: ErrorMessage
missingReturn = msgWithPos "Expected block to end with return"

invalidYieldType :: ErrorMessage
invalidYieldType = msgWithPos "Invalid sequence yield type"

usedBreakContinueOutOfLoop :: ErrorMessage
usedBreakContinueOutOfLoop = msgWithPos "Invalid usage of break; or continue; outside the loop scope"

divideByZero :: ErrorMessage
divideByZero = runtimeMsgWithPos "Cannot divide by zero"

modByZero :: ErrorMessage
modByZero = runtimeMsgWithPos "Cannot mod by zero"

indexOutOfBands :: Integer -> ErrorMessage
indexOutOfBands index = runtimeMsgWithPos $ "Array index out of bounds " ++ show index

fatalState :: ErrorMessage
fatalState = msgWithPos "Typechecker internal error: Fatal state in typechecking process"

msgWithPos :: String -> ErrorMessage
msgWithPos msg (Just (l, c)) = msg ++ " in line " ++ show l ++ " column " ++ show c
msgWithPos msg Nothing = msg ++ "not defined position"

runtimeMsgWithPos :: String -> ErrorMessage
runtimeMsgWithPos msg = msgWithPos $ "Runtime error: " ++ msg
