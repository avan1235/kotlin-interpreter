module Main where

import Control.Monad (forM_)
import Interpreter (interpret)
import Kotlin.Par (myLexer, pProgram)
import System.Environment (getArgs)
import TypeChecker (typecheck)
import Util (exitFailureMsg)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> handleInvalidUsage
    files -> forM_ files runInterpreterForFile

runInterpreterForFile :: String -> IO ()
runInterpreterForFile file = do
  text <- readFile file
  let tokens = myLexer text
  case pProgram tokens of
    Left err -> parseError err
    Right program -> do
      case typecheck program of
        Left err -> staticAnalysisError err
        _ -> do
          runResult <- interpret program
          case runResult of
            Left err -> runtimeError err
            _ -> return ()
  where
    parseError = exitFailureMsg file 1
    staticAnalysisError = exitFailureMsg file 2
    runtimeError = exitFailureMsg file 3

handleInvalidUsage :: IO ()
handleInvalidUsage = exitFailureMsg "invalid usage" 4 "Usage: ./interpreter <source_file> [<source_file>]"
