module Constants where

import Control.Monad.Except
import qualified Data.Map as Map
import qualified Data.Set as Set
import Kotlin.Abs
import TypeEnvironment

entryPoint :: (Ident, [CType], CType)
entryPoint = (Ident "main", [], CTUnit)

emptyTCEnv :: TCEnv
emptyTCEnv = (emptyTCVarEnv, emptyTCFunEnv, NoLoop)

emptyTCVarEnv :: TCVarEnv
emptyTCVarEnv = Map.empty

emptyTCFunEnv :: TCFunEnv
emptyTCFunEnv =
  Map.fromList
    [ ( named "println",
        Map.fromList
          [ sign [CTInt] CTUnit,
            sign [CTString] CTUnit,
            sign [CTBoolean] CTUnit
          ]
      )
    ]

emptyIEnv :: IEnv
emptyIEnv = (emptyIVarEnv, emptyIFunEnv)

emptyIFunEnv :: IFunEnv
emptyIFunEnv =
  Map.fromList
    [ ( named "println",
        Map.fromList
          [ println [CTInt] CTUnit,
            println [CTString] CTUnit,
            println [CTBoolean] CTUnit
          ]
      )
    ]

emptyIVarEnv :: IVarEnv
emptyIVarEnv = Map.empty

emptyIStore :: IStore
emptyIStore = Map.empty

println :: [CType] -> CType -> ([CType], (CType, HideState, IFun))
println ar rt = (ar, (rt, CanBeHidden, f))
  where
    showB True = "true"
    showB False = "false"
    f [IVInt i] ke s = lift (print i) >> ke IVUnit s
    f [IVString v] ke s = lift (putStrLn v) >> ke IVUnit s
    f [IVBoolean b] ke s = lift (putStrLn $ showB b) >> ke IVUnit s

emptyOI :: TCOutEnvInit
emptyOI = (id, Set.empty)

emptyState :: TCState
emptyState = ([], [], emptyOI)

intOp :: Op
intOp = ((CTInt, CTInt), CTInt)

booleanOp :: Op
booleanOp = ((CTBoolean, CTBoolean), CTBoolean)

stringOp :: Op
stringOp = ((CTString, CTString), CTString)

stringCompOp :: Op
stringCompOp = ((CTString, CTString), CTBoolean)

intCompOp :: Op
intCompOp = ((CTInt, CTInt), CTBoolean)

sign :: [CType] -> CType -> ([CType], (CType, HideState))
sign args rt = (args, (rt, CanBeHidden))

named :: String -> Ident
named = Ident

ek :: ICont
ek _ = throwError "Internal error - this continuation should not be used"

er :: IContR
er _ _ = throwError "Internal error - this return continuation should not be used"

ey :: IContY
ey _ _ = throwError "Internal error - this yield continuation should not be used"

ed :: IContD
ed _ _ = throwError "Internal error - this env change continuation should not be used"
